# brmi

Border Router Management Interface (BRMI) - Is a RESTful API to configure access, rules and configuration for the IoT border gateway orchestrated by BRO.